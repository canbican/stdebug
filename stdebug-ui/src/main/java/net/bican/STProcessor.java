package net.bican;

import java.util.HashMap;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupString;
import org.stringtemplate.v4.misc.STMessage;
import org.vaadin.aceeditor.AceEditor;

public class STProcessor implements STErrorListener {
  private static final String STRING_EMPTY = ""; //$NON-NLS-1$
  private static STGroup stgroup = null;
  private AceEditor errorsArea;
  
  public STProcessor(AceEditor errorsArea) {
    this.errorsArea = errorsArea;
    stgroup = new STGroupString(STRING_EMPTY);
    stgroup.delimiterStartChar = '$';
    stgroup.delimiterStopChar = '$';
    stgroup.setListener(this);
  }
  
  public String process(String input, HashMap<String, ?> object) {
    try {
      this.errorsArea.setReadOnly(false);
      this.errorsArea.setValue(""); //$NON-NLS-1$
      this.errorsArea.setReadOnly(true);
      ST st = new ST(stgroup, input);
      if ((object != null) && (object.keySet() != null)) {
        for (String o : object.keySet()) {
          st.add(o, object.get(o));
        }
      }
      String result = st.render();
      return result;
    } catch (Exception e) {
      return null;
    }
  }
  
  @Override
  public void compileTimeError(STMessage msg) {
    appendMessage(Messages.getString("compileerror"), msg.toString()); //$NON-NLS-1$
  }
  
  private void appendMessage(String prefix, String message) {
    this.errorsArea.setReadOnly(false);
    this.errorsArea.setValue(this.errorsArea.getValue() + prefix + ": " //$NON-NLS-1$
        + message + "\n"); //$NON-NLS-1$
    this.errorsArea.setReadOnly(true);
  }
  
  @Override
  public void runTimeError(STMessage msg) {
    appendMessage(Messages.getString("runerror"), msg.toString()); //$NON-NLS-1$
  }
  
  @Override
  public void IOError(STMessage msg) {
    appendMessage(Messages.getString("ioerror"), msg.toString()); //$NON-NLS-1$
  }
  
  @Override
  public void internalError(STMessage msg) {
    appendMessage(Messages.getString("internalerror"), msg.toString()); //$NON-NLS-1$
  }
  
}