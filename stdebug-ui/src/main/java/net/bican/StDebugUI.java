package net.bican;

import java.util.HashMap;

import javax.servlet.annotation.WebServlet;

import org.vaadin.aceeditor.AceEditor;
import org.yaml.snakeyaml.Yaml;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;

@SuppressWarnings("serial")
@Theme("stdebug")
public class StDebugUI extends UI implements TextChangeListener {
  
  private static final String STRING_EMPTY = ""; //$NON-NLS-1$
  
  @WebServlet(value = "/*", asyncSupported = true)
  @VaadinServletConfiguration(productionMode = false, ui = StDebugUI.class, widgetset = "net.bican.stdebugwidgetset")
  public static class Servlet extends VaadinServlet {
    // nothing yet
  }
  
  AceEditor templateInputArea;
  AceEditor variableInputArea;
  AceEditor resultArea;
  AceEditor errorsArea;
  private HashMap<String, ?> object = null;
  String initialTemplate = null;
  String initialVariables = null;
  
  @Override
  protected void init(VaadinRequest request) {
    this.setSizeFull();
    Page.getCurrent().setTitle(Messages.getString("windowtitle")); //$NON-NLS-1$
    VaadinSession.getCurrent().getSession().setMaxInactiveInterval(-1);
    
    Panel editorPanel = new Panel(Messages.getString("stringtemplatedebugger")); //$NON-NLS-1$
    editorPanel.setSizeFull();
    HorizontalSplitPanel hsplit = new HorizontalSplitPanel();
    hsplit.setSizeFull();
    VerticalSplitPanel vsplit = new VerticalSplitPanel();
    hsplit.setFirstComponent(vsplit);
    editorPanel.setContent(hsplit);
    
    this.resultArea = new AceEditor();
    this.resultArea.setImmediate(true);
    this.resultArea.setSizeFull();
    this.resultArea.setReadOnly(true);
    hsplit.setSecondComponent(this.resultArea);
    
    Panel templateInput = new Panel(Messages.getString("template")); //$NON-NLS-1$
    templateInput.setSizeFull();
    this.templateInputArea = new AceEditor();
    this.templateInputArea.setSizeFull();
    this.templateInputArea.addTextChangeListener(this);
    this.templateInputArea.setTextChangeTimeout(2000);
    this.templateInputArea.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
    this.templateInputArea.setImmediate(true);
    templateInput.setContent(this.templateInputArea);
    
    Panel variableInput = new Panel(Messages.getString("variables")); //$NON-NLS-1$
    variableInput.setSizeFull();
    this.variableInputArea = new AceEditor();
    this.variableInputArea.setSizeFull();
    this.variableInputArea.setTextChangeTimeout(2000);
    this.variableInputArea.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
    this.variableInputArea.addTextChangeListener(this);
    variableInput.setContent(this.variableInputArea);
    
    vsplit.addComponent(templateInput);
    vsplit.addComponent(variableInput);
    
    Panel errorPanel = new Panel(Messages.getString("errorsandwarnings")); //$NON-NLS-1$
    errorPanel.setSizeFull();
    this.errorsArea = new AceEditor();
    this.errorsArea.setSizeFull();
    this.errorsArea.setReadOnly(true);
    errorPanel.setContent(this.errorsArea);
    
    VerticalSplitPanel mainPanel = new VerticalSplitPanel();
    mainPanel.setSizeFull();
    mainPanel.setSplitPosition(80, Unit.PERCENTAGE);
    mainPanel.setFirstComponent(editorPanel);
    mainPanel.setSecondComponent(errorPanel);
    
    final VerticalLayout layout = new VerticalLayout();
    layout.setSizeFull();
    layout.addComponent(mainPanel);
    layout.setExpandRatio(mainPanel, 1);
    this.setContent(layout);
    
    String templateParameter = request.getParameter("template"); //$NON-NLS-1$
    String variableParameter = request.getParameter("variables"); //$NON-NLS-1$
    this.templateInputArea.setValue(templateParameter == null ? STRING_EMPTY
        : templateParameter);
    this.variableInputArea.setValue(variableParameter == null ? STRING_EMPTY
        : variableParameter);
    if (templateParameter != null) {
      processInput();
    }
  }
  
  @Override
  public void textChange(TextChangeEvent event) {
    AceEditor source = (AceEditor) event.getSource();
    uiUpdate(source, event.getText());
  }
  
  private void uiUpdate(AceEditor source, String newText) {
    source.setValue(newText);
    if (source == this.templateInputArea) {
      processInput();
    } else if (source == this.variableInputArea) {
      processVariables();
    }
  }
  
  @SuppressWarnings("unchecked")
  private void processVariables() {
    HashMap<String, ?> newObject;
    try {
      Yaml yaml = new Yaml();
      newObject = (HashMap<String, ?>) yaml.load(this.variableInputArea
          .getValue());
      this.object = newObject;
      processInput();
    } catch (Exception e) {
      this.errorsArea.setReadOnly(false);
      this.errorsArea
          .setValue(Messages.getString("variableserror") + e.getMessage()); //$NON-NLS-1$
      this.errorsArea.setReadOnly(true);
      return;
    }
  }
  
  private void processInput() {
    STProcessor processor = new STProcessor(this.errorsArea);
    String output = processor.process(this.templateInputArea.getValue(),
        this.object);
    if (output != null) {
      StDebugUI.this.resultArea.setReadOnly(false);
      StDebugUI.this.resultArea.setValue(output);
      StDebugUI.this.resultArea.setReadOnly(true);
    }
  }
}
